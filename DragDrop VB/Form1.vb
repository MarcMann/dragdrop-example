﻿Public Class Form1


    'Samle for http://msdn.microsoft.com/de-de/library/za0zx9y0%28v=vs.110%29.aspx?cs-save-lang=1&cs-lang=vb#code-snippet-1


    Enum eInternalDragState
        NONE = 0
        List1_Dragging = 1
        List2_Dragging = 2
    End Enum

    Enum eInternalDropState
        NONE = 0
        List1_Drop = 1
        List2_Drop = 2
    End Enum

    'Die beiden States speichern war hier grade so mit DragDrop veranstaltet wird
    Dim InternalDragState As eInternalDragState
    Dim InternalDropState As eInternalDropState

    Private Sub ListBox1_MouseDown(sender As Object, e As MouseEventArgs) Handles ListBox1.MouseDown

        'Bevor es hier überhaupt los geht prüfen wir ob überhaut etwas augewählt ist
        If ListBox1.SelectedItem = Nothing Then
            'Falls nicht ist hier ende
            Return
        End If

        Dim Result As DragDropEffects

        'Drag/Drop-States werden entspechend der aktuellen Aktion gesetzt
        InternalDragState = eInternalDragState.List1_Dragging
        InternalDropState = eInternalDropState.NONE
        'Der eigentliche DragDrop-Vorgang, ab hier haben wir keine kontrolle mehr was mit dem Objekt passiert
        'wir haben zwar noch mit auf den weg gegeben was damit gemacht werden darf, nur Move, aber es kann überall gedroppt werden
        'falls es ausßerhalb des Programms geproppt wird kann es da durchaus auch auftachen,
        'wir verhindern aber in der Resultauswertung das es dann hier verschwindet
        Result = ListBox1.DoDragDrop(ListBox1.SelectedItem, DragDropEffects.Move)
        'Aus DoDragDrop kommen wir erst zurück wenn das Element irgendwo gedroppt wurde
        'wir setzen also erstmal unseren internen Status zurück
        InternalDragState = eInternalDragState.NONE
        'und machen eine Resultauswertung von DoDragDrop
        Select Case Result
            Case DragDropEffects.None
                'nix passiert
            Case DragDropEffects.Copy
                'geht hier auch nicht
            Case DragDropEffects.Link
                'ebenfalls über
            Case DragDropEffects.Move
                'Ja das wollen wir
                'Das Element wird aber nur aus List1 entfernt, wenn das Target List2 war
                'List2 setzt praktischer weise den InternalDropState
                If (InternalDropState = eInternalDropState.List2_Drop) Then
                    ListBox1.Items.Remove(ListBox1.SelectedItem)
                End If
            Case DragDropEffects.Scroll
                'über
            Case Else
                'mir wumpe
        End Select
        'Und den InternalDropState zurücksetzen da wir durch sind
        InternalDropState = eInternalDropState.NONE
    End Sub

    Private Sub ListBox2_DragEnter(sender As Object, e As DragEventArgs) Handles ListBox2.DragEnter
        'Nur wenn das DragObjekt auch von List1 kommt wird es zugelassen...
        'Nur Move wird zugelassen
        If (InternalDragState = eInternalDragState.List1_Dragging) Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub ListBox2_DragDrop(sender As Object, e As DragEventArgs) Handles ListBox2.DragDrop
        'In DragEnter haben wir sichergestellt, dass das Element von List1 kommt, also fügen wir das bei uns ein
        ListBox2.Items.Add(e.Data.GetData(DataFormats.Text).ToString)
        'Und setzen den Internen DropState damit List1 auch entspechende Aktionen ausführen kann
        InternalDropState = eInternalDropState.List2_Drop
    End Sub


    '#########
    'Hier einfach nochmal das gleiche für den Rückweg --> Copy&Pase vom feinsten:
    '#########


    Private Sub ListBox2_MouseDown(sender As Object, e As MouseEventArgs) Handles ListBox2.MouseDown

        'Bevor es hier überhaupt los geht prüfen wir ob überhaut etwas augewählt ist
        If ListBox2.SelectedItem = Nothing Then
            'Falls nicht ist hier ende
            Return
        End If

        Dim Result As DragDropEffects

        'Drag/Drop-States werden entspechend der aktuellen Aktion gesetzt
        InternalDragState = eInternalDragState.List2_Dragging
        InternalDropState = eInternalDropState.NONE
        'Der eigentliche DragDrop-Vorgang, ab hier haben wir keine kontrolle mehr was mit dem Objekt passiert
        'wir haben zwar noch mit auf den weg gegeben was damit gemacht werden darf, nur Move, aber es kann überall gedroppt werden
        'falls es ausßerhalb des Programms geproppt wird kann es da durchaus auch auftachen,
        'wir verhindern aber in der Resultauswertung das es dann hier verschwindet
        Result = ListBox2.DoDragDrop(ListBox2.SelectedItem, DragDropEffects.Move)
        'Aus DoDragDrop kommen wir erst zurück wenn das Element irgendwo gedroppt wurde
        'wir setzen also erstmal unseren internen Status zurück
        InternalDragState = eInternalDragState.NONE
        'und machen eine Resultauswertung von DoDragDrop
        Select Case Result
            Case DragDropEffects.None
                'nix passiert
            Case DragDropEffects.Copy
                'geht hier auch nicht
            Case DragDropEffects.Link
                'ebenfalls über
            Case DragDropEffects.Move
                'Ja das wollen wir
                'Das Element wird aber nur aus List2 entfernt, wenn das Target List1 war
                'List1 setzt praktischer weise den InternalDropState
                If (InternalDropState = eInternalDropState.List1_Drop) Then
                    ListBox2.Items.Remove(ListBox2.SelectedItem)
                End If
            Case DragDropEffects.Scroll
                'über
            Case Else
                'mir wumpe
        End Select
        'Und den InternalDropState zurücksetzen da wir durch sind
        InternalDropState = eInternalDropState.NONE
    End Sub

    Private Sub ListBox1_DragEnter(sender As Object, e As DragEventArgs) Handles ListBox1.DragEnter
        'Nur wenn das DragObjekt auch von List2 kommt wird es zugelassen...
        'Nur Move wird zugelassen
        If (InternalDragState = eInternalDragState.List2_Dragging) Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub ListBox1_DragDrop(sender As Object, e As DragEventArgs) Handles ListBox1.DragDrop
        'In DragEnter haben wir sichergestellt, dass das Element von List2 kommt, also fügen wir das bei uns ein
        ListBox1.Items.Add(e.Data.GetData(DataFormats.Text).ToString)
        'Und setzen den Internen DropState damit List1 auch entspechende Aktionen ausführen kann
        InternalDropState = eInternalDropState.List1_Drop
    End Sub

End Class
